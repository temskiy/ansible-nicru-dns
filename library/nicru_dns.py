#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright: (c) 2022 Artem Gnatyuk <temskiy@gmail.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function
__metaclass__ = type

DOCUMENTATION = r'''
---
module: nicru_dns
author:
- Artem Gnatyuk (temskiy)
requirements:
   - python >= 2.6
   - nic-api
short_description: Manage nic.ru DNS records
description:
   - "Manages dns records via the nic.ru API, see the docs: U(https://www.nic.ru/help/upload/file/API_DNS-hosting.pdf)."
options:
  account_login:
    description:
    - Account username.
    - Required for token generation.
    type: str
    required: true
    aliases: [ login ]
  account_password:
    description:
    - Account password.
    - Required for token generation.
    type: str
    required: true
    aliases: [ password ]
  application_login:
    description:
    - Application login.
    - Required for application authentification.
    - Must be created as described in documetation.
    type: str
    required: true
    aliases: [ app_login ]
  application_password:
    description:
    - Application password.
    - Required for application authentification.
    - Must be created as described in documetation.
    type: str
    required: true
    aliases: [ app_password ]
  service_id:
    description:
    - nic.ru service identificator.
    type: str
    required: true
  zone:
    description:
    - DNS name to use (e.g. "example.com").
    - For cyrilic domain use punycode.
    type: str
    required: true
    aliases: [ domain ]
  record:
    description:
    - Record to add.
    - Required if C(state=present).
    - Default is C(@) (e.g. the zone name).
    type: str
    default: '@'
    aliases: [ name ]
  state:
    description:
    - Whether the record(s) should exist or not.
    type: str
    choices: [ absent, present ]
    default: present
  type:
    description:
      - The type of DNS record to create. Required if C(state=present).
    type: str
    choices: [ A, AAAA, CNAME, TXT ]
    required: true
  value:
    description:
    - The record value.
    - Required for C(state=present).
    type: str
    aliases: [ content ]
  ttl:
    description:
    - The TTL to give the new record.
    type: int
    default: 600
  force:
    description:
    - Create record if zone have uncommited changes.
    type: bool
    default: false
'''

EXAMPLES = r'''
- name: Add test record
  nicru_dns:
    service_id: "example_service_id"
    zone: "example.com"
    record: "_acme-test"
    type: "TXT"
    value: "test_record_content"

- name: Remove test record
  nicru_dns:
    state: absent
    service_id: "example_service_id"
    zone: "example.com"
    record: "_acme-test"
    type: "TXT"
'''

RETURN = ''

from nic_api import DnsApi
from nic_api.models import *
from ansible.module_utils.basic import AnsibleModule, env_fallback
from ansible.module_utils.urls import fetch_url

def api_init(app_login,app_password):
    oauth_config = {
        'APP_LOGIN': app_login,
        'APP_PASSWORD': app_password
    }
    return DnsApi(oauth_config)

def zone_has_changes(api,serviceid,zone):
    return api.zones(serviceid)['name' == zone].has_changes

def record_exists(api,serviceid,zone,record_name,record_type,record_content):
    records_all = api.records(serviceid, zone)
    records_name = filter(lambda record: record.name == record_name, records_all)

    if record_type.lower() == 'a':
        records = filter(lambda record: type(record) == ARecord and record.a == record_content, records_name)
    elif record_type.lower() == 'aaaa':
        records = filter(lambda record: type(record) == AAAARecord and record.aaaa == record_content, records_name)
    elif record_type.lower() == 'cname':
        records = filter(lambda record: type(record) == CNAMERecord and record.cname == record_content, records_name)
    elif record_type.lower() == 'txt':
        records = filter(lambda record: type(record) == TXTRecord and record.txt == record_content, records_name)
    records_list=list(records)

    if len(records_list) > 0:
        return True,records_list
    return False,records_list

def record_add(api,serviceid,zone,record_name,record_type,ttl,record_content):
    if record_type.lower() == 'a':
      record = ARecord(name=record_name, a=record_content, ttl=ttl)
    elif record_type.lower() == 'aaaa':
      record = AAAARecord(name=record_name, aaaa=record_content, ttl=ttl)
    elif record_type.lower() == 'cname':
      record = CNAMERecord(name=record_name, cname=record_content, ttl=ttl)
    elif record_type.lower() == 'txt':
      record = TXTRecord(name=record_name, txt=record_content, ttl=ttl)

    api.add_record(record, serviceid, zone)
    api.commit(serviceid, zone)

def record_delete(api,serviceid,zone,record_id):
    api.delete_record(record_id, serviceid, zone)
    api.commit(serviceid, zone)


def main():
    module = AnsibleModule(
        argument_spec=dict(
            account_login=dict(type="str", required=True, no_log=True, aliases=['login'], fallback=(env_fallback, ["NICRU_LOGIN"]),),
            account_password=dict(type="str", required=True, no_log=True, aliases=['password'], fallback=(env_fallback, ["NICRU_PASSWORD"]),),
            application_login=dict(type="str", required=True, no_log=True, aliases=['app_login'], fallback=(env_fallback, ["NICRU_APP_LOGIN"]),),
            application_password=dict(type="str", required=True, no_log=True, aliases=['app_password'], fallback=(env_fallback, ["NICRU_APP_PASSWORD"]),),
            service_id=dict(type='str', required=True),
            zone=dict(type='str', required=True, aliases=['domain']),
            record=dict(type='str', default='@', aliases=['name']),
            state=dict(type='str', default='present', choices=['absent', 'present']),
            type=dict(type='str', choices=['A', 'AAAA', 'CNAME', 'TXT']),
            value=dict(type='str', aliases=['content']),
            ttl=dict(type='int', default=600),
            force=dict(type='bool', default=False),
            
        ),
        supports_check_mode=False,
        required_if=[
            ('state', 'present', ['record', 'type', 'value']),
            ('state', 'absent', ['record']),
        ],
    )

    if (not module.params['account_login'] or not module.params['account_password'] or not module.params['application_login'] or not module.params['application_password']):
        module.fail_json(msg="Either account_login, account_password, application_login, application_password params are required.")

    changed = False
    
    nicru_api = api_init(module.params['application_login'],
                        module.params['application_password'])
    nicru_api.authorize(username=module.params['account_login'],
                        password=module.params['account_password'],
                        token_filename='nic_token.json')

    if zone_has_changes(nicru_api,module.params['service_id'],module.params['zone']) and not module.params['force']:
        module.fail_json(msg="Zone has uncommited changes")
    
    rec_exists,record = record_exists(nicru_api,module.params['service_id'],module.params['zone'],module.params['record'],module.params['type'],module.params['value'])
    if module.params['state'] == 'present':
        if rec_exists:
            changed = False
        else:
            record_add(nicru_api,module.params['service_id'],module.params['zone'],module.params['record'],module.params['type'],module.params['ttl'],module.params['value'])
            changed = True
        module.exit_json(changed=changed)        
    else:
        if rec_exists:
            record_delete(nicru_api,module.params['service_id'],module.params['zone'],record[0].id)
            changed = True
        else:
            changed = False
        module.exit_json(changed=changed)


if __name__ == '__main__':
    main()
